package com.devcamp.checknumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckNumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckNumberApplication.class, args);
	}

}
